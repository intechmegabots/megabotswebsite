module.exports = {
  ip: "127.0.0.1",
  port: 8000,
  debug: false,
  db: "mongodb://localhost:27017/megabots",
  trustProxy: false,
};