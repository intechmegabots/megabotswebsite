<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Megabots</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300,400italic' rel='stylesheet' type='text/css'> {{!Thanks GFonts for having to use a request}}
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> {{!TODO Do this right}}
    <style> {{!style at top for speed}}
      {{{s.css}}}
    </style>
  </head>
  <body class="{{#if editMode}}editMode{{/if}}">
    <div id="navbarContainer">
      <ul class="navbar">
        <a class="navbarBranding" href="/">
          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAeCAYAAADO4udXAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABDRJREFUeNrsW9GR4jAMFcw24CuB/cq3t4RQQighlAAlQAlLCesSziWQb74uJWxK4H7MjC9ryZKczP1YM5llIMiS/KwnKcvm+XxClSpLy7aGoEoFVpUKrCoVWFWqLC5vj8cj9b4BgD/h7xoyAsBHeK1dZwCAfXj9BQCtQscRAG7IZycAuCC2vyPfKYlbbEsXfNLqwGznyBkArsGHz2CLWMcb8kG7IqgAAHbRZZTA3APABAC/laA6E6Cyyo0piZuNdHwV6ugL9sYVHlYAALclArSmTCHjaNc5BB1WqcOFU4nJV+ZQwApx8wuAwoeY7JTfH8Kh3RX4MjRNM2LA6lYGlitY5xgCoN3IIejA5FKwMd0CwCoFZ/ef9uUfHW9IcEwiw7yHvy+5R6n3JddAMXFqvhMBSNHGr9k60o08Z7JRTt+JcZ9J2Mj1JxU3F+4pif1Lx3wPDAB8I1nfCTLvXC8AADRNw+4KWwQI04wOLJINchs/BYda5PtcUGE2eCWojKCuMsy4zf0xGZs5sc/pmMevR/bAE761jL0Vjxs6IkVyAJMLthOsI81WozQAkXwWUCDXn1ZBQZ4BFBBmH08c4E4IRBawsHTuBYCJadAKadAvvJFc6YV1xU4ZN4vYPBE6nEAHN/s4YQPiBUySBBbHaEMsnjNwijoXrFPkUtFSNLgjKHAUtvk5f6hNtkwa7AR+d5lGYRUaTAGrZyjlcnZHBKpHnHoS10lgw29Czz0xWjBIEzAwayyuPzaxhiN0cA6rlAYdkX2wOdwFi+fj8XhdOwxYmNIb43Ryi3sH+iGiY6RrYMxgYj0nws4rsQGGETfpiIFDpZ2ApqTZDdtbrrQUsLSpmFvce+Wcxke0lKsbcrXSLQriBbHzqA2oUGymo+RQqRfa5TIzvtIZ2A9g9coTIz1ZPTIj2RDXntEV+Qg4GySAMQ1g0/VjdA8nY3H8+chsBjf2HEbRZLfYnk3mSsa1aZopBSzL7Eg4nE0V1naBbjDXYEwMGsCm69eZzxPDllIK48beCoHSFsYZSuh1Szg+zLoibjdIzVlKZySGsJWTWV82nBAd5wVmV5JGhtLBaZqoblCS3Ur8TdLrVjjcK52zdIXOdpm6idPWY3XVAXmfmmNx/MEeCjuhDglQSp9qSOrJHzT4AhbmuKYjoVIlts4pM2Z4RvUQVpB+z+7HDgo2XT+G7DzX85mhQY4/d4INSnVIpudU0d4z9uAp0bslqEXTkeQoSCNDyCamQMcUNhALjCPoQ0LJ0g5qCR3cWRRVbowFcfUYsDgjfy5nU8V9qzT8sMCMZUDqqjEaLUjsMwVjhviZql1AB5cGh4w+FbhTNBhTYQ7d3Cf32H2tMoj76DSV1AjYf6oeMl0kBSyNP2NYcyzo0uY65nb1QhrUAutGzfs29QerVdaQ+iudKhVYVSqwqlRgVamyvPwdAIXRlZ0sQc90AAAAAElFTkSuQmCC"/>
        </a>
        {{#each navBarItems}}
          <li class="navItem{{#if subItems}} navParent{{/if}}">
            <a class="navItemLink" style="background-image:url('{{icon}}')" href="{{link}}">
              <div class="navItemText">
                {{{text}}}
              </div>
            </a>
            {{#if subItems}}
              <ul>
            {{/if}}
              {{#each subItems}}
                <li class="navItem navSubItem">
                  <a class="navItemLink" style="background-image:url('{{icon}}')" href="{{link}}">
                    <div class="navItemText">
                      {{{text}}}
                    </div>
                  </a>
                </li>
              {{/each}}
            {{#if subItems}}
              </ul>
            {{/if}}
          </li>
        {{/each}}
      </ul>
    </div>
    <div id="errorBar"></div>
    <table id="padTable"> {{! This is so terrible but it works so well}}
      <tr id="underNavbar">
        <td></td>
      </tr>
      <tr class="bodyHolder">
        <td id="body">
          <div id="content">
            
          </div>
        </td>
      </tr>
      <tr id="footer">
        <td  class="contentPadding">
          Footer Hello
        </td>
      </tr>
      {{#if editMode}}
        <tr id="underFooterPadding" class="editShow">
          <td></td>
        </tr>
        <tr id="underFooter" class="editHeight">
          <td></td>
        </tr>
      {{/if}}
    </table>
    {{#if editMode}}
      <div id="editFooter">
        <a id="editFooterButton" class="fa fa-fw fa-code noFollow editButton" href="//open/close"></a>
        <i id="editFooterResize" class="fa fa-fw fa-sort noFollow editButton hideDown" href="//resize"></i>
        <a id="editFooterSave" class="fa fa-fw fa-save noFollow editButton hideDown" href="//save"></a>
        <a id="editFooterReset" class="fa fa-fw fa-undo noFollow editButton hideDown" href="//reset"></a>
        <div id="editFooterContent" class="editHeight">
          Loading;
        </div>
      </div>
    {{/if}}
    <script>{{!script at bottom for speed}}
    {{#if debug}}var dbug = true;{{else}}var dbug = false;{{/if}}
    {{#if editMode}}var editMode = true;{{else}}var editMode = false;{{/if}}
    {{{s.scripts}}}
    </script>
  </body>
</html>