﻿/// <reference path="../common/node.d.ts" />
/// <reference path="../common/less.d.ts" />
/// <reference path="../common/handlebars.d.ts" />
/// <reference path="../common/interface.ts" />

import fs = require("fs");
import path = require("path");
import Handlebars = require("handlebars");
var uglifyJs = require("uglify-js");
var uglifyCss = require("uglifycss");
var htmlMinify = require("html-minifier").minify;
import less = require("less");

module.exports = (dbug, settings: compileSettings, callback: Function) =>
{
  var debug = (message?: any, ...optionalParams: any[]) => { }; // console.log null fn
  if (dbug) debug = console.log;

  var newSettings: compileSettings = { // init output dictionary
    css: [],
    scripts: [],
    editMode: settings.editMode,
    navBarItems: settings.navBarItems,
  };

  fs.readFile(path.join(__dirname,"/../../index.hb"), (err, indexHb) =>
  { //load main handlebars
    if (err) throw new Error("Cannot open " + path.join(__dirname,"/../../index.hb"));

    var complete = settings.css.length + settings.scripts.length; // setup completion checking
    var completeCallback = () =>
    {
      var newerSettings = {
        css: newSettings.css.join("\n"),
        scripts: newSettings.scripts.join("\n"),
      }
    if (!dbug) // minify in production
      {
        newerSettings.scripts = uglifyJs.minify(newerSettings.scripts, {
          fromString: true,
          mangle: false,
          inline_script: true,
        }).code;
        newerSettings.css = uglifyCss.processString(newerSettings.css, {
          maxLineLen: 16000,
        });
      }
      var filledHtml = Handlebars.compile(indexHb.toString())({
        debug: dbug,
        s: newerSettings,
        editMode: newSettings.editMode,
        navBarItems: settings.navBarItems,
      }); // Handlebars compile and execute
      if (!dbug) // minify in production
      {
        filledHtml = htmlMinify(filledHtml, {
          removeComments: true,
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeRedundantAttributes: true,
          keepClosingSlash: true,
          caseSensitive: true,
        });
      }
      callback(filledHtml); // exit point
    };

    settings.css.forEach((name, index) => // add all of the stylesheets to the settings array
    {
      var file = path.join( __dirname,"/../../less/",name);
      fs.exists(file, (exists) => // less exists check
      {
        if (!exists) // less does not exist
        {
          file = path.join(__dirname,"/../../css/", name);
          fs.exists(file, (exists) => // check raw css
          {
            debug("Loading " + file);
            if (!exists) throw new Error("Cannot open " + name); // raw css does not exist; throw error
            var fileContents = fs.readFileSync(file);
            if (!fileContents) throw new Error("Cannot open " + name);
            debug("Loaded " + file);
            newSettings.css[index] = (fileContents.toString()); //push onto list
            complete--; // check if others are complete
            if (complete == 0) completeCallback();

          });
        }
        else // less does exist
        {
          debug("Loading " + file);
          var fileContents = fs.readFileSync(file); // load file
          less.render(fileContents.toString(), (err, output) => // render
          {
            if (err) throw err;
            debug("Loaded " + file);
            newSettings.css[index] = (output.css); // push onto list
            complete--; // check if others are complete
            if (complete == 0) completeCallback();
          });
        }
      });
    });

    settings.scripts.forEach((name, index) => // add all of the scripts to the settings array (and handebars fragments)
    {
      var file = path.join( __dirname,"/../client/",name);
      fs.exists(file, (exists) => // compiled typescript check
      {
        if (!exists) // compiled typescript does not exists
        {
          file = path.join( __dirname ,"/../../js/", name);
          fs.exists(file, (exists) =>
          {
            if (!exists) // normal js does not exits
            {
              file = path.join( __dirname, "/../../pages/", name);
              fs.exists(file, (exists) => //handlebars fragment exists check
              {
                if (!exists) throw new Error("Cannot open " + name);
                debug("Loading " + file); //handlebars fragment exists
                var fileContents = fs.readFileSync(file); // copy from handlebars/bin/handlebars ~lime 150
                var output = ['(function() {\n', '  var template = Handlebars.template, templates = '];
                output.push('Handlebars.templates');
                output.push(' = ');
                output.push('Handlebars.templates');
                output.push(' || {};\n');
                output.push('templates[\'' + name + '\'] = template(' + Handlebars.precompile(fileContents.toString(), {}) + ');\n');
                output.push('})();');
                debug("Loaded " + file);
                newSettings.scripts[index] = output.join(''); // push onto list
                complete--; // check if others are complete
                if (complete == 0) completeCallback();
              });
            }
            else
            {
              debug("Loading " + file);
              var fileContents = fs.readFileSync(file);
              if (!fileContents) throw new Error("Cannot open " + name);
              debug("Loaded " + file);
              newSettings.scripts[index] = (fileContents.toString()); // push onto list
              complete--; // check if others are complete
              if (complete == 0) completeCallback();
            }
          });
        }
        else // compiled typescript exists
        {
          debug("Loading " + file);
          var fileContents = fs.readFileSync(file);
          if (!fileContents) throw new Error("Cannot open " + name);
          debug("Loaded " + file);
          newSettings.scripts[index] = (fileContents.toString()); // push onto list
          complete--; // check if others are complete
          if (complete == 0) completeCallback();
        }
      });
    });
  });
};