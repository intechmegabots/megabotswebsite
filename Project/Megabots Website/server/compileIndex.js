﻿define(["require", "exports", "fs", "path", "handlebars", "less"], function(require, exports, fs, path, Handlebars, less) {
    var uglifyJs = require("uglify-js");
    var uglifyCss = require("uglifycss");
    var htmlMinify = require("html-minifier").minify;

    module.exports = function (dbug, settings, callback) {
        var debug = function (message) {
            var optionalParams = [];
            for (var _i = 0; _i < (arguments.length - 1); _i++) {
                optionalParams[_i] = arguments[_i + 1];
            }
        };
        if (dbug)
            debug = console.log;

        var newSettings = {
            css: [],
            scripts: [],
            editMode: settings.editMode,
            navBarItems: settings.navBarItems
        };

        fs.readFile(path.join(__dirname, "/../../index.hb"), function (err, indexHb) {
            if (err)
                throw new Error("Cannot open " + path.join(__dirname, "/../../index.hb"));

            var complete = settings.css.length + settings.scripts.length;
            var completeCallback = function () {
                var newerSettings = {
                    css: newSettings.css.join("\n"),
                    scripts: newSettings.scripts.join("\n")
                };
                if (!dbug) {
                    newerSettings.scripts = uglifyJs.minify(newerSettings.scripts, {
                        fromString: true,
                        mangle: false,
                        inline_script: true
                    }).code;
                    newerSettings.css = uglifyCss.processString(newerSettings.css, {
                        maxLineLen: 16000
                    });
                }
                var filledHtml = Handlebars.compile(indexHb.toString())({
                    debug: dbug,
                    s: newerSettings,
                    editMode: newSettings.editMode,
                    navBarItems: settings.navBarItems
                });
                if (!dbug) {
                    filledHtml = htmlMinify(filledHtml, {
                        removeComments: true,
                        collapseWhitespace: true,
                        collapseBooleanAttributes: true,
                        removeRedundantAttributes: true,
                        keepClosingSlash: true,
                        caseSensitive: true
                    });
                }
                callback(filledHtml);
            };

            settings.css.forEach(function (name, index) {
                var file = path.join(__dirname, "/../../less/", name);
                fs.exists(file, function (exists) {
                    if (!exists) {
                        file = path.join(__dirname, "/../../css/", name);
                        fs.exists(file, function (exists) {
                            debug("Loading " + file);
                            if (!exists)
                                throw new Error("Cannot open " + name);
                            var fileContents = fs.readFileSync(file);
                            if (!fileContents)
                                throw new Error("Cannot open " + name);
                            debug("Loaded " + file);
                            newSettings.css[index] = (fileContents.toString());
                            complete--;
                            if (complete == 0)
                                completeCallback();
                        });
                    } else {
                        debug("Loading " + file);
                        var fileContents = fs.readFileSync(file);
                        less.render(fileContents.toString(), function (err, output) {
                            if (err)
                                throw err;
                            debug("Loaded " + file);
                            newSettings.css[index] = (output.css);
                            complete--;
                            if (complete == 0)
                                completeCallback();
                        });
                    }
                });
            });

            settings.scripts.forEach(function (name, index) {
                var file = path.join(__dirname, "/../client/", name);
                fs.exists(file, function (exists) {
                    if (!exists) {
                        file = path.join(__dirname, "/../../js/", name);
                        fs.exists(file, function (exists) {
                            if (!exists) {
                                file = path.join(__dirname, "/../../pages/", name);
                                fs.exists(file, function (exists) {
                                    if (!exists)
                                        throw new Error("Cannot open " + name);
                                    debug("Loading " + file);
                                    var fileContents = fs.readFileSync(file);
                                    var output = ['(function() {\n', '  var template = Handlebars.template, templates = '];
                                    output.push('Handlebars.templates');
                                    output.push(' = ');
                                    output.push('Handlebars.templates');
                                    output.push(' || {};\n');
                                    output.push('templates[\'' + name + '\'] = template(' + Handlebars.precompile(fileContents.toString(), {}) + ');\n');
                                    output.push('})();');
                                    debug("Loaded " + file);
                                    newSettings.scripts[index] = output.join('');
                                    complete--;
                                    if (complete == 0)
                                        completeCallback();
                                });
                            } else {
                                debug("Loading " + file);
                                var fileContents = fs.readFileSync(file);
                                if (!fileContents)
                                    throw new Error("Cannot open " + name);
                                debug("Loaded " + file);
                                newSettings.scripts[index] = (fileContents.toString());
                                complete--;
                                if (complete == 0)
                                    completeCallback();
                            }
                        });
                    } else {
                        debug("Loading " + file);
                        var fileContents = fs.readFileSync(file);
                        if (!fileContents)
                            throw new Error("Cannot open " + name);
                        debug("Loaded " + file);
                        newSettings.scripts[index] = (fileContents.toString());
                        complete--;
                        if (complete == 0)
                            completeCallback();
                    }
                });
            });
        });
    };
});
//# sourceMappingURL=compileIndex.js.map
