/// <reference path="../common/node.d.ts" />
/// <reference path="../common/express.d.ts" />
/// <reference path="../common/interface.ts" />
/// <reference path="../common/mongodb.d.ts" />
/// <reference path="../common/passport.d.ts" />
/// <reference path="compileIndex.ts" />

import express = require("express");
import mongo = require("mongodb");
import path = require("path");
var bodyParser = require("body-parser");
var MongoClient = mongo.MongoClient;
var importedSettings = require("../../settings.js");
var compileIndex: Function = require("./compileIndex.js");
var forceNonDebug = false;
 
var settings: Settings = { // defaults for settings
  ip: importedSettings.ip || "127.0.0.1",
  port: importedSettings.port || 8000,
  debug: process.argv[2] === "debug" || importedSettings.debug || false,
  db: importedSettings.db || "mongodb://localhost:27017/megabots",
  trustProxy: importedSettings.trustProxy || false,
};
if (forceNonDebug)
  settings.debug = false;

var scriptList = [ // scripts for / (and handlebars fragments)
  "native.history.js",
  "prevel-full.js",
  "handlebars.runtime-v2.0.0.js",
  "HandlebarHelper.js",
  "404.hb",
  "blogMain.hb",
  "blog.hb",
  "index.js" //actually a ts but compilation
];
var lessList = [ // css for /
  "ResetCss20.css",
  "vars.less",
  "navbar.less"
];
var editScriptList = [ // scripts for / in editmode
  "blogMainEdit.hb",
  "blogEdit.hb",
  "edit.js"
];
var editLessList = [ // css for / in editmode
  "edit.less"
];
var navBarItems: navBarItem[] = [
  {
    text: "c",
    icon: "",
    link: "/",
    subItems: [
      {
        text: "a",
        icon: "",
        link: "/",
      },
      {
        text: "b",
        icon: "",
        link: "/",
      },
      {
        text: "c",
        icon: "",
        link: "/",
      },
    ]
  },
  {
    text: "b",
    icon: "",
    link: "/",
  },
  {
    text: "a",
    icon: "",
    link: "/",
  }

];


interface Express_RequestFunction
{
  (req: express.Request, res: Response, next: Function): any;
}

module Blog
{
  export function toMini(blog:Blog): MiniBlog
  {
    return {
      title: blog.title,
      date: blog.date,
      contents: blog.contents,
      author: User.OTW(blog.author),
      url: "/blog/" + blog.urlName,
      locked: blog.locked,
    };
  };
  export function OTW(blog:Blog): OTWBlog
  {
    var b = {
      title: blog.title,
      date: blog.date,
      contents: blog.contents,
      author: User.OTW(blog.author),
      url: "/blog/" + blog.urlName,
      comments: [],
      locked: blog.locked,
    };
    this.comments && this.comments.forEach((e, i) =>
    {
      b.comments[i] = e.OTW();
    });
    return b;
  };
  export function getDefault(): Blog
  {
    return {
      title: "New Blog",
      date: Date.now(),
      contents: "Hello World",
      comments: [],
      author: undefined, //TODO author
      urlName: "Hello",
      locked: true,
    };
  };
}
module User
{
  export function OTW(user:User): OTWUser
  {
    if (!user)
      return user;
    return {
      name: user.name||"",
      image: user.image||"",
    };
  };
}
module BlogComment
{
  export function OTW(comment:BlogComment): OTWComment
  {
    return {
      name: comment.name,
      contents: comment.contents,
      date: comment.creation.date,
      locked: false,
    };
  };
}

function atob(a: any)
{
  if (typeof (a) === "boolean") return a;
  if (typeof (a) === "string")
  {
    if ((<string>a).toLowerCase() == "true") return true;
    return false;
  }
  return !!a;
}

var debug = (message?: any, ...optionalParams: any[]) => { }; // fast debug function
if (settings.debug)
{
  debug = console.log;
}

debug("Starting in debug mode");

MongoClient.connect(settings.db, (err, db) => // Connect to the database 
{
  if (err) throw err;
  debug("Connected to database.");
  var app = express();

  if (settings.debug) // set express's options
  {
    app.set("etag", false); // disable caching in dev mode
    app.set("env", "development");
  }
  else
  {
    app.set("etag", true); // caching
    app.set("env", "production");
  }
  app.set("trust proxy", settings.trustProxy); // trust NGINX/Apache's reverse proxy support

  var isAuthenticated = (req:express.Request)=>
  {
    //TODO Authenticate
    return true;
  };
  var authenticateRequest:Express_RequestFunction = (req, res, next) =>
  {
    if (isAuthenticated(req))
    {
      next();
    }
    else
    {
      return;
    }
  };


  app.get("/api/s/:file", (req, res, next) =>
  {
    res.sendfile(path.join(__dirname, "/../../sFiles/", req.params.file));
  });

  // blog
  db.collection("blog", (err, blogCollection) =>
  { 
    if (err) throw err;
    app.get(/^\/api\/([0-9]*)\/?$/, (req, res, next) => // /api/ :number (must be 0-9 number)
    {
      var BLOG_PER_PAGE = 10;
      var pageAdd: number = BLOG_PER_PAGE * (Number(req.params[0] || 0));
      var query:any = { locked: false};
      if (isAuthenticated(req))
        query = {};
      blogCollection.find(query, { limit: BLOG_PER_PAGE, skip: pageAdd }).toArray((err, result) =>
      {
        if (err) return next();
        var blogs: MiniBlog[] = [];
        result.forEach((item:Blog,i) =>
        {
          if (!item) return;
          if (err) return debug(err);
          blogs[i] = (Blog.toMini(item));
        });
        var response: API = {
          title: "Home",
          indexETag: "",
          response: blogs,
          url: "/",
          handler: "blogMain.hb",
        };
        res.send(response);
      });
    });
    app.get("/api/blog/:name", (req, res, next) =>
    {
      blogCollection.findOne({ urlName: req.params.name }, (err, result: Blog) =>
      {
        if (err)
        {
          err.place = "blog.get.findOne";
          return res.send({ error: err });
        }
        if (!result)
        {
          return next();
        }
        var blog= Blog.OTW(result);
        var response: API = {
          title: blog.title,
          indexETag: "",
          response: blog,
          url: blog.url,
          handler: "blog.hb",
        };
        res.send(response);
      });
    });
    app.post("/api/blog/create", authenticateRequest, (req, res, next) =>
    {
      blogCollection.count((err,count:number) =>
      {
        if (err)
        {
          err.place = "blog.create.count";
          return res.send({ error: err });
        }
        var newBlog: Blog = Blog.getDefault();
        newBlog.urlName = count.toString();
        debug(newBlog);
        blogCollection.insert([newBlog], (err:any, ids) =>
        {
          if (err)
          {
            err.place = "blog.create.insert";
            return res.send({ error: err });
          }
          return res.send({id:count});
        });
      });
    });
    app.post("/api/blog/:name", authenticateRequest, bodyParser.urlencoded({extended:false}) , (req, res, next) =>
    {
      var newEntrys = {};
      debug(req.body);
      for(var k in req.body)
      {
        if (typeof k == "number") // index not key
        continue;
        switch (k) // set types properly
        {
          case "title":
          case "contents":
          case "urlName":
            newEntrys[k] = req.body[k];
            break;
          case "date":
            newEntrys[k] = Number(req.body[k]);
            break;
          case "locked":
            newEntrys[k] = atob(req.body[k]);
            break;
          default:
            break;
        }
      }
      debug(newEntrys);
      blogCollection.findAndModify({ urlName: req.params.name }, [], { $set: newEntrys }, (err:any, doc) =>
      {
        if (err)
        {
          err.place = "blog.edit.findAndModify";
          return res.send({ error: err });
        }
        var response: API = {
          title: doc.title,
          indexETag: "",
          handler: "blog.hb",
          response: Blog.OTW(doc),
          url:doc.url,
        };
        res.send(response)
      });
    });
  });

  app.get(/^\/api\/(.*)$/, (req, res, next) => // 404 error
  {
    var url = req.path.slice(req.path.indexOf("api/") + 3/* 3= `api`.length*/); // remove the first api/ keep the /
    var Error404: OTW404 = {
      url: url,
      locked: false,
    };
    var response: API = {
      title: "Error 404",
      indexETag: "",
      url: url,
      handler: "404.hb",
      response: Error404,
    };
    res.send(response);
  });

  if (settings.debug) // /home
  {
    app.get(/(.*)/, (req, res, next) => // debug; compile for every request        also any path. (All /api/s have been grabbed by now)
    {
      var opts = { scripts: scriptList, css: lessList, editMode: isAuthenticated(req), navBarItems: navBarItems };
      if (isAuthenticated(req))
      {
        opts = { scripts: scriptList.concat(editScriptList), css: lessList.concat(editLessList), editMode: isAuthenticated(req), navBarItems: navBarItems }
      }
      compileIndex(true, opts, (compiled) =>
      {
        res.send(compiled);
      });
    });
  }
  else
  {// production; compile on start
    compileIndex(false, { scripts: scriptList.concat(editScriptList), css: lessList.concat(editLessList), editMode: true, navBarItems: navBarItems }, (compiledEditMode) => 
    {
      console.log("Compiled 1/2");
      compileIndex(false, { scripts: scriptList, css: lessList, editMode: false, navBarItems: navBarItems }, (compiled) =>
      {
        console.log("Compiled 2/2");
        app.get(/(.*)/, (req, res, next) => 
        {
          if (isAuthenticated(compiledEditMode)) 
            res.status(200).send(compiledEditMode);
          else
            res.status(200).send(compiled);
        });
      });
    });
  }

  app.listen(settings.port, settings.ip, function () // start server
  {
    console.log("Server has started on port 8000.");
  });
});