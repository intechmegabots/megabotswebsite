define(["require", "exports", "express", "mongodb", "path"], function(require, exports, express, mongo, path) {
    var bodyParser = require("body-parser");
    var MongoClient = mongo.MongoClient;
    var importedSettings = require("../../settings.js");
    var compileIndex = require("./compileIndex.js");
    var forceNonDebug = false;

    var settings = {
        ip: importedSettings.ip || "127.0.0.1",
        port: importedSettings.port || 8000,
        debug: process.argv[2] === "debug" || importedSettings.debug || false,
        db: importedSettings.db || "mongodb://localhost:27017/megabots",
        trustProxy: importedSettings.trustProxy || false
    };
    if (forceNonDebug)
        settings.debug = false;

    var scriptList = [
        "native.history.js",
        "prevel-full.js",
        "handlebars.runtime-v2.0.0.js",
        "HandlebarHelper.js",
        "404.hb",
        "blogMain.hb",
        "blog.hb",
        "index.js"
    ];
    var lessList = [
        "ResetCss20.css",
        "vars.less",
        "navbar.less"
    ];
    var editScriptList = [
        "blogMainEdit.hb",
        "blogEdit.hb",
        "edit.js"
    ];
    var editLessList = [
        "edit.less"
    ];
    var navBarItems = [
        {
            text: "c",
            icon: "",
            link: "/",
            subItems: [
                {
                    text: "a",
                    icon: "",
                    link: "/"
                },
                {
                    text: "b",
                    icon: "",
                    link: "/"
                },
                {
                    text: "c",
                    icon: "",
                    link: "/"
                }
            ]
        },
        {
            text: "b",
            icon: "",
            link: "/"
        },
        {
            text: "a",
            icon: "",
            link: "/"
        }
    ];

    var Blog;
    (function (Blog) {
        function toMini(blog) {
            return {
                title: blog.title,
                date: blog.date,
                contents: blog.contents,
                author: User.OTW(blog.author),
                url: "/blog/" + blog.urlName,
                locked: blog.locked
            };
        }
        Blog.toMini = toMini;
        ;
        function OTW(blog) {
            var b = {
                title: blog.title,
                date: blog.date,
                contents: blog.contents,
                author: User.OTW(blog.author),
                url: "/blog/" + blog.urlName,
                comments: [],
                locked: blog.locked
            };
            this.comments && this.comments.forEach(function (e, i) {
                b.comments[i] = e.OTW();
            });
            return b;
        }
        Blog.OTW = OTW;
        ;
        function getDefault() {
            return {
                title: "New Blog",
                date: Date.now(),
                contents: "Hello World",
                comments: [],
                author: undefined,
                urlName: "Hello",
                locked: true
            };
        }
        Blog.getDefault = getDefault;
        ;
    })(Blog || (Blog = {}));
    var User;
    (function (User) {
        function OTW(user) {
            if (!user)
                return user;
            return {
                name: user.name || "",
                image: user.image || ""
            };
        }
        User.OTW = OTW;
        ;
    })(User || (User = {}));
    var BlogComment;
    (function (BlogComment) {
        function OTW(comment) {
            return {
                name: comment.name,
                contents: comment.contents,
                date: comment.creation.date,
                locked: false
            };
        }
        BlogComment.OTW = OTW;
        ;
    })(BlogComment || (BlogComment = {}));

    function atob(a) {
        if (typeof (a) === "boolean")
            return a;
        if (typeof (a) === "string") {
            if (a.toLowerCase() == "true")
                return true;
            return false;
        }
        return !!a;
    }

    var debug = function (message) {
        var optionalParams = [];
        for (var _i = 0; _i < (arguments.length - 1); _i++) {
            optionalParams[_i] = arguments[_i + 1];
        }
    };
    if (settings.debug) {
        debug = console.log;
    }

    debug("Starting in debug mode");

    MongoClient.connect(settings.db, function (err, db) {
        if (err)
            throw err;
        debug("Connected to database.");
        var app = express();

        if (settings.debug) {
            app.set("etag", false);
            app.set("env", "development");
        } else {
            app.set("etag", true);
            app.set("env", "production");
        }
        app.set("trust proxy", settings.trustProxy);

        var isAuthenticated = function (req) {
            return true;
        };
        var authenticateRequest = function (req, res, next) {
            if (isAuthenticated(req)) {
                next();
            } else {
                return;
            }
        };

        app.get("/api/s/:file", function (req, res, next) {
            res.sendfile(path.join(__dirname, "/../../sFiles/", req.params.file));
        });

        db.collection("blog", function (err, blogCollection) {
            if (err)
                throw err;
            app.get(/^\/api\/([0-9]*)\/?$/, function (req, res, next) {
                var BLOG_PER_PAGE = 10;
                var pageAdd = BLOG_PER_PAGE * (Number(req.params[0] || 0));
                var query = { locked: false };
                if (isAuthenticated(req))
                    query = {};
                blogCollection.find(query, { limit: BLOG_PER_PAGE, skip: pageAdd }).toArray(function (err, result) {
                    if (err)
                        return next();
                    var blogs = [];
                    result.forEach(function (item, i) {
                        if (!item)
                            return;
                        if (err)
                            return debug(err);
                        blogs[i] = (Blog.toMini(item));
                    });
                    var response = {
                        title: "Home",
                        indexETag: "",
                        response: blogs,
                        url: "/",
                        handler: "blogMain.hb"
                    };
                    res.send(response);
                });
            });
            app.get("/api/blog/:name", function (req, res, next) {
                blogCollection.findOne({ urlName: req.params.name }, function (err, result) {
                    if (err) {
                        err.place = "blog.get.findOne";
                        return res.send({ error: err });
                    }
                    if (!result) {
                        return next();
                    }
                    var blog = Blog.OTW(result);
                    var response = {
                        title: blog.title,
                        indexETag: "",
                        response: blog,
                        url: blog.url,
                        handler: "blog.hb"
                    };
                    res.send(response);
                });
            });
            app.post("/api/blog/create", authenticateRequest, function (req, res, next) {
                blogCollection.count(function (err, count) {
                    if (err) {
                        err.place = "blog.create.count";
                        return res.send({ error: err });
                    }
                    var newBlog = Blog.getDefault();
                    newBlog.urlName = count.toString();
                    debug(newBlog);
                    blogCollection.insert([newBlog], function (err, ids) {
                        if (err) {
                            err.place = "blog.create.insert";
                            return res.send({ error: err });
                        }
                        return res.send({ id: count });
                    });
                });
            });
            app.post("/api/blog/:name", authenticateRequest, bodyParser.urlencoded({ extended: false }), function (req, res, next) {
                var newEntrys = {};
                debug(req.body);
                for (var k in req.body) {
                    if (typeof k == "number")
                        continue;
                    switch (k) {
                        case "title":
                        case "contents":
                        case "urlName":
                            newEntrys[k] = req.body[k];
                            break;
                        case "date":
                            newEntrys[k] = Number(req.body[k]);
                            break;
                        case "locked":
                            newEntrys[k] = atob(req.body[k]);
                            break;
                        default:
                            break;
                    }
                }
                debug(newEntrys);
                blogCollection.findAndModify({ urlName: req.params.name }, [], { $set: newEntrys }, function (err, doc) {
                    if (err) {
                        err.place = "blog.edit.findAndModify";
                        return res.send({ error: err });
                    }
                    var response = {
                        title: doc.title,
                        indexETag: "",
                        handler: "blog.hb",
                        response: Blog.OTW(doc),
                        url: doc.url
                    };
                    res.send(response);
                });
            });
        });

        app.get(/^\/api\/(.*)$/, function (req, res, next) {
            var url = req.path.slice(req.path.indexOf("api/") + 3);
            var Error404 = {
                url: url,
                locked: false
            };
            var response = {
                title: "Error 404",
                indexETag: "",
                url: url,
                handler: "404.hb",
                response: Error404
            };
            res.send(response);
        });

        if (settings.debug) {
            app.get(/(.*)/, function (req, res, next) {
                var opts = { scripts: scriptList, css: lessList, editMode: isAuthenticated(req), navBarItems: navBarItems };
                if (isAuthenticated(req)) {
                    opts = { scripts: scriptList.concat(editScriptList), css: lessList.concat(editLessList), editMode: isAuthenticated(req), navBarItems: navBarItems };
                }
                compileIndex(true, opts, function (compiled) {
                    res.send(compiled);
                });
            });
        } else {
            compileIndex(false, { scripts: scriptList.concat(editScriptList), css: lessList.concat(editLessList), editMode: true, navBarItems: navBarItems }, function (compiledEditMode) {
                console.log("Compiled 1/2");
                compileIndex(false, { scripts: scriptList, css: lessList, editMode: false, navBarItems: navBarItems }, function (compiled) {
                    console.log("Compiled 2/2");
                    app.get(/(.*)/, function (req, res, next) {
                        if (isAuthenticated(compiledEditMode))
                            res.status(200).send(compiledEditMode);
                        else
                            res.status(200).send(compiled);
                    });
                });
            });
        }

        app.listen(settings.port, settings.ip, function () {
            console.log("Server has started on port 8000.");
        });
    });
});
//# sourceMappingURL=index.js.map
