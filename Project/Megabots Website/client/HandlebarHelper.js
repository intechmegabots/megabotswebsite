﻿function nn(n) {
    if (n < 10)
        return "0" + n;
    return n.toString();
}

Handlebars.registerHelper("date", function (stringTime) {
    var time = new Date(Number(stringTime));
    var currentTime = new Date();
    if (currentTime.toDateString() === time.toDateString()) {
        return new Handlebars.SafeString("Today, " + time.getHours() + ":" + nn(time.getMinutes()));
    } else {
        return new Handlebars.SafeString(time.getMonth() + "\\" + time.getDay() + "\\" + (time.getFullYear() - 2000) + ", " + time.getHours() + ":" + nn(time.getMinutes()));
    }
});
//# sourceMappingURL=HandlebarHelper.js.map
