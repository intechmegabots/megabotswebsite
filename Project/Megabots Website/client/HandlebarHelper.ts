﻿/// <reference path="../common/handlebars.d.ts" />
// Its like Hamburger Helper!

function nn(n: number): string
{
  if (n < 10)
    return "0" + n;
  return n.toString();
}

Handlebars.registerHelper("date", (stringTime: string) =>
{
  var time: Date = new Date(Number(stringTime));
  var currentTime = new Date();
  if (currentTime.toDateString() === time.toDateString())// same day 
  {
    return new Handlebars.SafeString("Today, " + time.getHours() + ":" + nn(time.getMinutes()));
  }
  else
  {
    return new Handlebars.SafeString(time.getMonth() +"\\"+ time.getDay() + "\\" + (time.getFullYear()-2000) + ", " + time.getHours() + ":" + nn(time.getMinutes()));
  }
});
