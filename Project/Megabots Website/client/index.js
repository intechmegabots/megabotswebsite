﻿var HistoryJS = History;

var currentResponse;
var apiUrl;

var debug = function (message) {
    var optionalParams = [];
    for (var _i = 0; _i < (arguments.length - 1); _i++) {
        optionalParams[_i] = arguments[_i + 1];
    }
};
if (dbug) {
    debug = function () {
        var args = [];
        for (var _i = 0; _i < (arguments.length - 0); _i++) {
            args[_i] = arguments[_i + 0];
        }
        console.log.apply(console, arguments);
    };
}

function loadPage(page) {
    if (page.lastIndexOf(window.location.origin) != -1)
        page = page.substr(page.lastIndexOf(window.location.origin) + window.location.origin.length);
    if (/^https?:\/\/(.*)$/.test(page)) {
        window.location.href = page;
        return;
    }
    while (page[0] == "/")
        page = page.substr(1, page.length);
    apiUrl = "/api/" + page;
    debug("Loading " + apiUrl);
    pl.ajax({
        url: apiUrl,
        dataType: "json",
        success: function (api) {
            HistoryJS.replaceState(null, api.title + " - Megabots", api.url);
            debug(api);
            currentResponse = api;
            pl('#content').get(0).innerHTML = (Handlebars.templates[api.handler] || (function (r) {
                return "Error 404";
            }))(api.response);
            if (onLoadHbs[api.handler])
                onLoadHbs[api.handler]();
            document.title = api.title + " - Megabots";
            if (editMode) {
                var editHandler = api.handler.replace(".hb", "Edit.hb");
                pl('#editFooterContent').get(0).innerHTML = (Handlebars.templates[editHandler] || (function (r) {
                    return "Error 404";
                }))(api.response);
                if (onLoadHbs[editHandler])
                    onLoadHbs[editHandler]();
            }
        },
        error: function (num, text) {
            error("Cannot retreive " + page + ": " + num);
        }
    });
}
function error(error) {
    console.log(error);
    var el = document.createElement("div");
    pl(el).addClass('error');
    el.innerHTML = "Error: " + error + "<a href='' class='noFollow closeParent fa fa-times'></a>";
    pl('#errorBar').append(el);
    setTimeout(function () {
        el.style.height = "1em";
    }, 100);
}
function notify(error) {
    console.log(error);
    var el = document.createElement("div");
    pl(el).addClass('notification');
    el.innerHTML = "" + error + "<a href='' class='noFollow closeParent fa fa-times'></a>";
    pl('#errorBar').append(el);
    setTimeout(function () {
        el.style.height = "1em";
    }, 100);
}
function anytob(a) {
    if (typeof (a) === "boolean")
        return a;
    if (typeof (a) === "string") {
        if (a.toLowerCase() == "true")
            return true;
        return false;
    }
    return a;
}
window.addEventListener('popstate', function (event) {
    loadPage(HistoryJS.getState().url);
});
document.body.addEventListener("click", function (event) {
    if (event.target.tagName) {
        var el = event.target;
        while (el.tagName.toLowerCase() != "a" && el.tagName.toLowerCase() != "button") {
            el = el.parentElement;
            if (el.tagName.toLowerCase() == "body")
                return;
        }
        if (pl(el).hasClass("noFollow")) {
            if (editMode) {
                if (editNoFollow(event, el)) {
                }
            }
            if (pl(el).hasClass("closeParent")) {
                event.preventDefault();
                el.parentElement.parentElement.removeChild(el.parentElement);
            }
            return;
        }
        event.preventDefault();
        var page = el.href;
        HistoryJS.pushState(null, null, page);
        loadPage(page);
    }
});
var onLoadHbs = [];
if (!editMode)
    loadPage(window.location.pathname);
//# sourceMappingURL=index.js.map
