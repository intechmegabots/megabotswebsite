﻿//Only loaded in edit mode
var editor;
module EditBar // Editbar controls
{
  var editWindowHeight = 0; // -1 = 50% 0= hidden >0 = npx heigh
  export function show()
  {
    var eFooter = pl('#editFooter');
    eFooter.addClass('editUp');
    localStorage["editUp"] = editWindowHeight = -1;
    pl(".editShow").css("display","table-cell");
    reflow();
  };
  export function hide()
  {
    var eFooter = pl('#editFooter');
    eFooter.removeClass('editUp');
    localStorage["editUp"] = editWindowHeight = 0;
    pl(".editShow").css("display","none");
    reflow();
  };
  export function setSize(size: number)
  {
    localStorage["editUp"] = editWindowHeight = size;
    reflow();
  };
  export function getSize(): number
  {
    if (editWindowHeight == -1)
      return document.documentElement.clientHeight * .45;
    return editWindowHeight;
  };
  export function reflow()
  {
    pl(".editHeight").css({ height: (getSize()) + "px" });
    editor && editor.resize();
  };
}

function editNoFollow(event: MouseEvent, el: HTMLElement)
{
  if (el.id == "editFooterButton")
  {
    event.preventDefault();
    var eFooter = pl('#editFooter');//toggle editUp
    if (eFooter.hasClass('editUp'))
    {
      EditBar.hide()
    }
    else
    {
      EditBar.show();
    }
  }
  else if (el.id === "composeButton")
  {
    event.preventDefault();
    pl.ajax({
      url: '/api/' + pl('#composeSelect').get(0).selectedOptions[0].value + '/create',
      type: 'POST',
      dataType: 'json',
      success: (res, code) =>
      {
        if (res.error)
          return error("Cannot create " + pl('#composeSelect').get(0).selectedOptions[0].value + ": " + JSON.stringify(res.error));
        loadPage("/blog/" + res.id);
      },
      error: (num, err) =>
      {
        error("Cannot create " + pl('#composeSelect').get(0).selectedOptions[0].value + ": " + num);
      },
    });
  }
  else if (el.id == "editFooterSave")
  {
    event.preventDefault();
    pl.ajax({
      url: apiUrl,
      type: 'POST',
      dataType: 'json',
      data: editToPartialType(),
      success: (res, code) =>
      {
        notify("Saved!");
      },
      error: (code, err) =>
      {
        error("Cannot update "+apiUrl+" ");
      }

    });
  }
  else if (el.id == "editFooterReset")
  {
    event.preventDefault();

  }
  else
  {
    return false;
  }
  return true;
}

var e = document.createElement("script");
e.src = "//cdnjs.cloudflare.com/ajax/libs/ace/1.1.3/ace.js";
e.onload = () =>
{
  loadPage(window.location.pathname);
};

pl("body").get(0).appendChild(e)

window.onresize = EditBar.reflow;

pl("#editFooterResize").get(0).addEventListener("mousedown", (event: MouseEvent) =>
{
  event.stopPropagation();
  var startY = event.pageY;
  var posStart = EditBar.getSize();
  pl("#editFooterResize").addClass("grabbing");
  var moveListener = (event: MouseEvent) =>
  {
    event.stopPropagation();
    var dY = event.pageY - startY;
    EditBar.setSize(posStart - dY);
  }
  var upListener = (event: MouseEvent) =>
  {
    event.stopPropagation();
    document.removeEventListener("mouseup", upListener);
    document.removeEventListener("mousemove", moveListener);
    pl("#editFooterResize").removeClass("grabbing");
  };
  document.addEventListener('mousemove', moveListener);
  document.addEventListener('mouseup', upListener);
}); 

var editToPartialType: Function = () => { };

onLoadHbs["blogEdit.hb"] = () =>
{
  editor = ace.edit('editContents');
  editor.setTheme('ace/theme/twilight');
  editor.getSession().setMode('ace/mode/html');
  editor.getSession().setTabSize(2);
  editor.setValue((<OTWBlog>currentResponse.response).contents);
  editToPartialType = ():Blog => 
  { 
    var n: Blog = <Blog>{};
    n.title = pl("#editTitle").get(0).value;
    n.urlName = /^(?:\/blog\/)?(.*)$/.exec(pl("#editUrl").get(0).value)[1];
    n.locked = pl("#editLocked").get(0).checked;
    n.contents = editor.getValue();
    return n;
  };
  var renderNew = () =>
  {
    var opts = pl.extend(editToPartialType(),currentResponse.response);
    pl('#content').get(0).innerHTML = ((Handlebars.templates["blog.hb"] || ((r) => { return "Error 404"; }))(opts));
  }
  pl("#editFooterContent input:not(#editContents)").each(function ()
  {
    (<HTMLInputElement>this).addEventListener("keyup", renderNew);
  });
  editor.getSession().on('change', renderNew);
  setTimeout(() => { EditBar.reflow(); }, 100);
};

pl(".editShow").css("display", "none");
if (Number(localStorage["editUp"]))
{
  EditBar.setSize(Number(localStorage["editUp"]));
  EditBar.show();
}
EditBar.reflow();