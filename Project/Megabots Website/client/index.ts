﻿/// <reference path="../common/handlebars.d.ts" />
/// <reference path="../common/prevel.d.ts" />
/// <reference path="../common/history.d.ts" />
/// <reference path="../common/interface.ts" />

// pl = prevel  - https://github.com/chernikovalexey/Prevel/tree/master/Docs
var HistoryJS: Historyjs = <any>History; // typescript crutch
declare var ace: any;

declare var dbug: boolean;
declare var editMode: boolean;// declared by handlebars in index.hb

var currentResponse: API;
var apiUrl: string;

var debug = (message?: any, ...optionalParams: any[]) => { }; // fast debug function
if (dbug)
{
  debug = (...args:any[]) => { console.log.apply(console, arguments) };
}

interface Location // fix lib.d.ts
{
  origin: string;
}

function loadPage(page: string):void
{
  if (page.lastIndexOf(window.location.origin) != -1) // remove the http://abc.def/ if it exists
    page = page.substr(page.lastIndexOf(window.location.origin) + window.location.origin.length);
  if (/^https?:\/\/(.*)$/.test(page)) // external sites
  {
    window.location.href = page;
    return;
  }
  while (page[0] == "/") page = page.substr(1, page.length); // remove leading `/`s
  apiUrl = "/api/" + page;
  debug("Loading " + apiUrl);
  pl.ajax({
    url: apiUrl,
    dataType: "json",
    success: function (api:API)
    {
      HistoryJS.replaceState(null, api.title + " - Megabots", api.url); // change URL bar
      debug(api);
      currentResponse = api; // move into global namespace
      pl('#content').get(0).innerHTML = (Handlebars.templates[api.handler] || ((r) => { return "Error 404"; }))(api.response); // render and add to DOM Tree
      if (onLoadHbs[api.handler]) // run any template js
        onLoadHbs[api.handler]();
      document.title = api.title + " - Megabots"; // change title
      if (editMode)
      {
        var editHandler = api.handler.replace(".hb", "Edit.hb"); // edit modo;
        pl('#editFooterContent').get(0).innerHTML = (Handlebars.templates[editHandler] || ((r) => { return "Error 404"; }))(api.response); // render edit panel and add to Dom Tree
        if (onLoadHbs[editHandler]) // template js
          onLoadHbs[editHandler]();
      }
    },
    error: function (num, text)
    {
      error("Cannot retreive "+page+": "+num);
    },
  });
}
function error(error: string):void
{ // display error to user
  console.log(error);
  var el: HTMLDivElement = document.createElement("div");
  pl(el).addClass('error');
  el.innerHTML = "Error: " + error + "<a href='' class='noFollow closeParent fa fa-times'></a>";
  pl('#errorBar').append(el);
  setTimeout(() => { el.style.height = "1em"; },100); // animate
}
function notify(error: string): void
{ // success
  console.log(error);
  var el: HTMLDivElement = document.createElement("div");
  pl(el).addClass('notification');
  el.innerHTML = "" + error + "<a href='' class='noFollow closeParent fa fa-times'></a>";
  pl('#errorBar').append(el);
  setTimeout(() => { el.style.height = "1em"; }, 100); // animate
}
function anytob(a: any): boolean // any to boolean
{
  if (typeof (a) === "boolean") return a;
  if (typeof (a) === "string")
  {
    if ((<string>a).toLowerCase() == "true") return true;
    return false;
  }
  return a;
}
window.addEventListener('popstate', (event:Event)=>  // back button
{ 
  loadPage(HistoryJS.getState().url);
});
document.body.addEventListener("click", (event: MouseEvent) => // any click on any element
{
  if ((<HTMLElement>event.target).tagName) // is an element
  {
    var el: HTMLElement = <HTMLElement>event.target;
    while (el.tagName.toLowerCase() != "a" && el.tagName.toLowerCase() != "button") // check if the element has a <a> or <button> as a  parent
    {
      el = el.parentElement;
      if (el.tagName.toLowerCase() == "body")
        return; // no <a> / <button>
    }
    if (pl(el).hasClass("noFollow"))
    {
      if (editMode)
      {
        if (editNoFollow(event,el))
           { }
      }
      if (pl(el).hasClass("closeParent")) // success / error
      {
        event.preventDefault();
        el.parentElement.parentElement.removeChild(el.parentElement);
      }
      return;
    } // this is a link
    event.preventDefault(); // prevent link from being followed
    var page = (<HTMLAnchorElement>el).href;
    HistoryJS.pushState(null, null, page); // update url
    loadPage(page); // load
  }
});
var onLoadHbs: Function[] = [];
if (!editMode) // editMode will load for itself.
  loadPage(window.location.pathname); 