interface API
{
  indexETag: string;
  handler: string;
  response: Response;
  url: string;
  title: string;
}
interface Response
{
}
interface User
{
  name: string;
  loginName: string;
  password: string;
  lastLogin: Login;
  currentLogin: Login;
  image: string;
  permissions: Permission[];
  email: string;
}
interface OTWUser
{
  name: string;
  image: string;
}
interface Blog extends Response
{
  title: string;
  date: number;
  contents: string;
  comments: Comment[];
  author: User;
  urlName: string;
  locked: boolean;
}
interface MiniBlog extends Response
{
  title: string;
  date: number;
  contents: string;
  author: OTWUser;
  url: string;
  locked: boolean;
}
interface OTWBlog extends Response
{
  title: string;
  date: number;
  contents: string;
  comments: OTWComment[];
  author: OTWUser;
  url: string;
  locked: boolean;
}
interface Login
{
  date: number;
  ip: string;
}
interface BlogComment extends Response
{
  name: string;
  contents: string;
  creation: Login;
  locked: boolean;
}
interface OTWComment extends Response
{
  name: string;
  contents: string;
  date: number;
  locked: boolean;
}
interface Permission
{
  has: boolean;
  name: string;
}
interface Settings
{
  ip: string;
  port: number;
  db: string;
  debug: boolean;
  trustProxy: boolean;
};
interface compileSettings
{
  scripts: string[];
  css: string[];
  editMode: boolean;
  navBarItems: navBarItem[];
}
interface OTW404 extends Response
{
  url: string;
  locked: boolean;
};
interface navBarItem
{
  text: string;
  icon: string;
  link: string;
  subItems?: navBarItem[];
};