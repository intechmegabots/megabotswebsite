/// <reference path="../common/handlebars.d.ts" />
/// <reference path="../common/prevel.d.ts" />
/// <reference path="../common/history.d.ts" />
/// <reference path="../common/interface.ts" />
// pl = prevel  - https://github.com/chernikovalexey/Prevel/tree/master/Docs
var HistoryJS = History; // typescript crutch
var currentResponse;
var apiUrl;
var debug = function (message) {
    var optionalParams = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        optionalParams[_i - 1] = arguments[_i];
    }
}; // fast debug function
if (dbug) {
    debug = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        console.log.apply(console, arguments);
    };
}
function loadPage(page) {
    if (page.lastIndexOf(window.location.origin) != -1)
        page = page.substr(page.lastIndexOf(window.location.origin) + window.location.origin.length);
    if (/^https?:\/\/(.*)$/.test(page)) {
        window.location.href = page;
        return;
    }
    while (page[0] == "/")
        page = page.substr(1, page.length);
    apiUrl = "/api/" + page;
    debug("Loading " + apiUrl);
    pl.ajax({
        url: apiUrl,
        dataType: "json",
        success: function (api) {
            HistoryJS.replaceState(null, api.title + " - Megabots", api.url); // change URL bar
            debug(api);
            currentResponse = api; // move into global namespace
            pl('#content').get(0).innerHTML = (Handlebars.templates[api.handler] || (function (r) {
                return "Error 404";
            }))(api.response); // render and add to DOM Tree
            if (onLoadHbs[api.handler])
                onLoadHbs[api.handler]();
            document.title = api.title + " - Megabots"; // change title
            if (editMode) {
                var editHandler = api.handler.replace(".hb", "Edit.hb"); // edit modo;
                pl('#editFooterContent').get(0).innerHTML = (Handlebars.templates[editHandler] || (function (r) {
                    return "Error 404";
                }))(api.response); // render edit panel and add to Dom Tree
                if (onLoadHbs[editHandler])
                    onLoadHbs[editHandler]();
            }
        },
        error: function (num, text) {
            error("Cannot retreive " + page + ": " + num);
        }
    });
}
function error(error) {
    console.log(error);
    var el = document.createElement("div");
    pl(el).addClass('error');
    el.innerHTML = "Error: " + error + "<a href='' class='noFollow closeParent fa fa-times'></a>";
    pl('#errorBar').append(el);
    setTimeout(function () {
        el.style.height = "1em";
    }, 100); // animate
}
function notify(error) {
    console.log(error);
    var el = document.createElement("div");
    pl(el).addClass('notification');
    el.innerHTML = "" + error + "<a href='' class='noFollow closeParent fa fa-times'></a>";
    pl('#errorBar').append(el);
    setTimeout(function () {
        el.style.height = "1em";
    }, 100); // animate
}
function anytob(a) {
    if (typeof (a) === "boolean")
        return a;
    if (typeof (a) === "string") {
        if (a.toLowerCase() == "true")
            return true;
        return false;
    }
    return a;
}
window.addEventListener('popstate', function (event) {
    loadPage(HistoryJS.getState().url);
});
document.body.addEventListener("click", function (event) {
    if (event.target.tagName) {
        var el = event.target;
        while (el.tagName.toLowerCase() != "a" && el.tagName.toLowerCase() != "button") {
            el = el.parentElement;
            if (el.tagName.toLowerCase() == "body")
                return; // no <a> / <button>
        }
        if (pl(el).hasClass("noFollow")) {
            if (editMode) {
                if (editNoFollow(event, el)) {
                }
            }
            if (pl(el).hasClass("closeParent")) {
                event.preventDefault();
                el.parentElement.parentElement.removeChild(el.parentElement);
            }
            return;
        } // this is a link
        event.preventDefault(); // prevent link from being followed
        var page = el.href;
        HistoryJS.pushState(null, null, page); // update url
        loadPage(page); // load
    }
});
var onLoadHbs = [];
if (!editMode)
    loadPage(window.location.pathname);
