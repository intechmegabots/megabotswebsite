/// <reference path="../common/node.d.ts" />
/// <reference path="../common/less.d.ts" />
/// <reference path="../common/handlebars.d.ts" />
/// <reference path="../common/interface.ts" />
var fs = require("fs");
var path = require("path");
var Handlebars = require("handlebars");
var uglifyJs = require("uglify-js");
var uglifyCss = require("uglifycss");
var htmlMinify = require("html-minifier").minify;
var less = require("less");
module.exports = function (dbug, settings, callback) {
    var debug = function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
    }; // console.log null fn
    if (dbug)
        debug = console.log;
    var newSettings = {
        css: [],
        scripts: [],
        editMode: settings.editMode,
        navBarItems: settings.navBarItems
    };
    fs.readFile(path.join(__dirname, "/../../index.hb"), function (err, indexHb) {
        if (err)
            throw new Error("Cannot open " + path.join(__dirname, "/../../index.hb"));
        var complete = settings.css.length + settings.scripts.length; // setup completion checking
        var completeCallback = function () {
            var newerSettings = {
                css: newSettings.css.join("\n"),
                scripts: newSettings.scripts.join("\n")
            };
            if (!dbug) {
                newerSettings.scripts = uglifyJs.minify(newerSettings.scripts, {
                    fromString: true,
                    mangle: false,
                    inline_script: true
                }).code;
                newerSettings.css = uglifyCss.processString(newerSettings.css, {
                    maxLineLen: 16000
                });
            }
            var filledHtml = Handlebars.compile(indexHb.toString())({
                debug: dbug,
                s: newerSettings,
                editMode: newSettings.editMode,
                navBarItems: settings.navBarItems
            }); // Handlebars compile and execute
            if (!dbug) {
                filledHtml = htmlMinify(filledHtml, {
                    removeComments: true,
                    collapseWhitespace: true,
                    collapseBooleanAttributes: true,
                    removeRedundantAttributes: true,
                    keepClosingSlash: true,
                    caseSensitive: true
                });
            }
            callback(filledHtml); // exit point
        };
        settings.css.forEach(function (name, index) {
            var file = path.join(__dirname, "/../../less/", name);
            fs.exists(file, function (exists) {
                if (!exists) {
                    file = path.join(__dirname, "/../../css/", name);
                    fs.exists(file, function (exists) {
                        debug("Loading " + file);
                        if (!exists)
                            throw new Error("Cannot open " + name); // raw css does not exist; throw error
                        var fileContents = fs.readFileSync(file);
                        if (!fileContents)
                            throw new Error("Cannot open " + name);
                        debug("Loaded " + file);
                        newSettings.css[index] = (fileContents.toString()); //push onto list
                        complete--; // check if others are complete
                        if (complete == 0)
                            completeCallback();
                    });
                }
                else {
                    debug("Loading " + file);
                    var fileContents = fs.readFileSync(file); // load file
                    less.render(fileContents.toString(), function (err, output) {
                        if (err)
                            throw err;
                        debug("Loaded " + file);
                        newSettings.css[index] = (output.css); // push onto list
                        complete--; // check if others are complete
                        if (complete == 0)
                            completeCallback();
                    });
                }
            });
        });
        settings.scripts.forEach(function (name, index) {
            var file = path.join(__dirname, "/../client/", name);
            fs.exists(file, function (exists) {
                if (!exists) {
                    file = path.join(__dirname, "/../../js/", name);
                    fs.exists(file, function (exists) {
                        if (!exists) {
                            file = path.join(__dirname, "/../../pages/", name);
                            fs.exists(file, function (exists) {
                                if (!exists)
                                    throw new Error("Cannot open " + name);
                                debug("Loading " + file); //handlebars fragment exists
                                var fileContents = fs.readFileSync(file); // copy from handlebars/bin/handlebars ~lime 150
                                var output = ['(function() {\n', '  var template = Handlebars.template, templates = '];
                                output.push('Handlebars.templates');
                                output.push(' = ');
                                output.push('Handlebars.templates');
                                output.push(' || {};\n');
                                output.push('templates[\'' + name + '\'] = template(' + Handlebars.precompile(fileContents.toString(), {}) + ');\n');
                                output.push('})();');
                                debug("Loaded " + file);
                                newSettings.scripts[index] = output.join(''); // push onto list
                                complete--; // check if others are complete
                                if (complete == 0)
                                    completeCallback();
                            });
                        }
                        else {
                            debug("Loading " + file);
                            var fileContents = fs.readFileSync(file);
                            if (!fileContents)
                                throw new Error("Cannot open " + name);
                            debug("Loaded " + file);
                            newSettings.scripts[index] = (fileContents.toString()); // push onto list
                            complete--; // check if others are complete
                            if (complete == 0)
                                completeCallback();
                        }
                    });
                }
                else {
                    debug("Loading " + file);
                    var fileContents = fs.readFileSync(file);
                    if (!fileContents)
                        throw new Error("Cannot open " + name);
                    debug("Loaded " + file);
                    newSettings.scripts[index] = (fileContents.toString()); // push onto list
                    complete--; // check if others are complete
                    if (complete == 0)
                        completeCallback();
                }
            });
        });
    });
};
