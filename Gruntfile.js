module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-typescript');  // includes
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-supervisor');
    grunt.loadNpmTasks('grunt-concurrent');
 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        typescript: { // recompile, not clean the entire codebase
            base: {
                src: [
                  'Project/Megabots Website/*.ts',
                  'Project/Megabots Website/*/*.ts'
                  ],
                dest: 'compiled/',
                options: {
                  module: 'commonjs',
                  basePath:'Project/Megabots Website', // dont prefix this to all of the files, just server/client
                }
            }
        },
        clean: [ // delete the compiled directory
          "compiled/"
        ],
        watch: { // watch any source for changes and recompile, not clean the scripts
            files: [
                  'Project/Megabots Website/*.ts',
                  'Project/Megabots Website/*/*.ts'
                  ],
            tasks: ['typescript'],
            options: {
              interrupt: true, // can kill if hanging
            }
        },
        supervisor: { 
          run: { // production start
            script: "compiled/server/index.js",
            options: {
              args: [],
              watch: ['Gruntfile.js'], // dont actually reload the server
              exec: 'node',
              quiet: true,
              pollInterval:1000*60*60 // long poll (1 hour)
            }
          },
          debug: { // debug start
            script: "compiled/server/index.js",
            options: {
              args: ['debug'], // pass debug to node
              watch: ['compiled/server'], // watch for changes
              exec: 'node', 
              quiet: false,
              pollInterval:500
            }
          }
        },
        concurrent: { // run watch and the server simultaneously 
          hotStart: {
            tasks: ['watch','supervisor:debug'],
            options: {
              logConcurrentOutput:true, // echo what node and typestript say
            },
          }
        },
    });
    
    grunt.registerTask('default',function() { // `$ grunt` command
      grunt.log.writeln("Starting node using precompiled scripts");
      grunt.task.run('start')
    });
    
    // register our tasks
    grunt.registerTask('start','Starts node using precompiled everything. This is for a production enviroment',['supervisor:run']);
    grunt.registerTask('debug','Starts node in debug mode. This will watch for changes and recompile on the fly.',['compile','concurrent:hotStart']);
    grunt.registerTask('compile','Compiles everything needed for a production server',['clean','typescript']);
};