<div class="contentPadding">
  {{#each this}}
  <div class="blog">
    <a class="blog.title" href="{{this.url}}">{{this.title}}</a><br>
    <img class="blog.author.image" src="/api/image/{{this.author.image}}" alt="{{this.author.name}}'s Image">
    <span class="blog.author.name"> {{this.author.name}} </span>
    <span class="blog.date"> - {{date this.date}} </span><br>
    {{{this.contents}}}
  </div>
  {{/each}}
</div>