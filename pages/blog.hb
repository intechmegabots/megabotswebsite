<div class="contentPadding">
  <div class="blog">
    <a class="blog.title" href="{{this.url}}">{{this.title}}</a><br>
    <img class="blog.author.image" src="/api/image/{{this.author.image}}" alt="{{this.author.name}}'s Image">
    <span class="blog.author.name"> {{this.author.name}} </span>
    <span class="blog.date"> - {{date this.date}} </span><br>
    {{{this.contents}}}
  </div>
  <div class="shelf"><div class="a"></div><div class="b"></div></div>
  {{#each comment}}
  <div class="comment">
    <span class="comment.name"> {{this.name}} </span>
    <span class="blog.date"> - {{date this.date}} </span><br>
    {{{this.contents}}}
  </div>
  {{else}}
    There are no comments.
  {{/each}}
</div>