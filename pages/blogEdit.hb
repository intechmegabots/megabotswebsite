<table class="editTable">
  <tr>
         <td>Title
    </td><td><input type="text" id="editTitle" value="{{title}}"/>
  </td></tr><tr>
         <td>URL Slug
    </td><td><input type="text" id="editUrl" value="{{url}}"/>
  </td></tr><tr>
         <td>Locked
    </td><td><input type="checkbox" id="editLocked" value="true" {{#if locked}}checked{{/if}}></input>
  </td></tr><tr>
         <td>Author
    </td><td>{{author.name}}
  </td></tr><tr>
         <td>Date
    </td><td>{{date date}}
  </td></tr><tr>
         <td>
    </td><td><div id='editContents' class="editHeight"></div>
  </td></tr>
</table>